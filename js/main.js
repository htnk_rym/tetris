document.getElementById("hello_text").textContent = "はじめてのJavaScript";

var count = 0;

var cells; //ゲーム盤を示す変数

//ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1,1,1,1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1,1],
            [1,1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0,1,0],
            [1,1,1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0,1,1],
            [1,1,0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1,1,0],
            [0,1,1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1,0,0],
            [1,1,1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0,0,1],
            [1,1,1]
        ]
    }
};

loadTable(); //ゲーム盤を読み込む


var timer = setInterval(function(){
    
    // キーボードイベントを監視する
    document.addEventListener("keydown", onKeyDown);
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
    if (hasFallingBlock()){ //落下中のブロックがあるか確認する
        fallBlocks(); //あればブロックを落とす
    }else{ //なければ
        deleteRow(); //そろっている行を消す
        generateBlock(); //ランダムにブロックを作成する
    }
    // ブロックが積み上がり切っていないかのチェック
    for (var row = 0; row < 2; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className !== "" && isFalling === false) {
                clearInterval(timer); 
                document.removeEventListener("keydown", onKeyDown);
                alert("gameover");
            }
        }
    }
},1000);





/* ココから下は関数の宣言部分 */

function loadTable(){
    //ゲーム盤を変数cellsにまとめる
    cells = [];
    var td_array = document.getElementsByTagName("td"); //200個の要素をを持つ配列
    var index = 0;
    for (var row = 0; row < 20; row++){
        cells[row] = []; //配列のそれぞれの要素を配列にする（2次元配列にする）
        for (var col = 0; col < 10; col++){
            cells[row][col] = td_array[index];
            cells[row][col].blockNum = null;
            index++;
        }
    }
}

function fallBlocks(){
    //ブロックを落とすプログラムを記述する
    // 1.底についていないか
    for (var col = 0; col < 10; col++){
        if (cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            document.removeEventListener("keydown", onKeyDown);
            return; //一番下の行にブロックがいるので落とさない
        }
    }
    // 2.1マス下に別のブロックがないか
    for (var row = 18; row >= 0; row--){
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){ //左下から今落ちているブロックがあるマスを特定する
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){ //下が空のマスで無く、かつ一緒に落ちているブロックでない時
                    isFalling = false;
                    document.removeEventListener("keydown", onKeyDown);
                    return; //一つ下のマスにブロックがいるので落とさない
                }
            }

        }
    }
    //ブロックを落とす
    //下から二番目の行から繰り返しクラスを下げていく
    for(var row = 18; row >= 0; row--){
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){ //今落ちているブロックだけ一列下げる
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;
function hasFallingBlock(){
    //落下中のブロックがあるか確認する
    return isFalling;
}

function deleteRow(){
    //そろっている行を消す
    for (var row = 19; row > 0; row--) {
        var canDelete = true;
        var deleteDoble = true;
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className === "") {
                canDelete = false;
            }
        }
        if (canDelete) {
            //2行消せるか確認
            for (var col=0; col < 10; col++){
                if(cells[row - 1][col].className === ""){
                    deleteDoble = false;
                }
            }
            if(deleteDoble){
                // 2行消す
                for (var col = 0; col < 10; col++) {
                    cells[row][col].className = "";
                    cells[row - 1][col].className = "";
                }
                // 2行上の行のブロックをすべて2行落とす
                for (var downRow = row - 2; downRow >= 0; downRow--) {
                    for (var col = 0; col < 10; col++) {
                        cells[downRow + 2][col].className = cells[downRow][col].className;
                        cells[downRow + 2][col].blockNum = cells[downRow][col].blockNum;
                        cells[downRow][col].className = "";
                        cells[downRow][col].blockNum = null;
                    }
                }
            }else{
                // 1行消す
                for (var col = 0; col < 10; col++) {
                    cells[row][col].className = "";
                }
                // 上の行のブロックをすべて1行(今ループが回っている行に)落とす
                for (var downRow = row - 1; downRow >= 0; downRow--) {
                    for (var col = 0; col < 10; col++) {
                        cells[downRow + 1][col].className = cells[downRow][col].className;
                        cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                        cells[downRow][col].className = "";
                        cells[downRow][col].blockNum = null;
                    }
                }
            }
            return;
        }
    }
}

var fallingBlockNum = 0;
function generateBlock(){
    //ランダムにブロックを生成する
    // 1.ブロックパターンからランダムに一つパターンを選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    // 2.選んだパターンを基にブロックを配置する
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++){
        for (var col = 0; col < pattern[row].length; col++){
            if (pattern[row][col]){
                cells[row][col + 3].className = nextBlock.class;    //[col + 3] 4行目からブロックを生成
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    // 3.落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
    if (event.keyCode === 37) {
        moveLeft();
    } else if (event.keyCode === 39) {
        moveRight();
    }
}

function moveRight(){
    // 1.右側の壁に接している
    for (var row = 19; row >= 0; row--) {
        if (cells[row][9].blockNum === fallingBlockNum){
            return; //右側の壁に落ちているブロックが接しているので動かさない
        }
    }
    // 2.右側に別のブロックがある
    for (var row = 19; row >= 0; row--){
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){ //今落ちているブロックで
                /* 右側が空でなく、かつ落ちているブロックではない */
                if (cells[row][col + 1].blockNum !== null && cells[row][col + 1].blockNum !== fallingBlockNum){
                    return;
                }
            }
        }
    }
    //ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLeft(){
    // 1.左側の壁に接している
    for (var row = 19; row >= 0; row--) {
        if (cells[row][0].blockNum === fallingBlockNum){
            return; //右側の壁に落ちているブロックが接しているので動かさない
        }
    }
    // 2.左側に別のブロックがある
    for (var row = 19; row >= 0; row--){
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum){ //今落ちているブロックで
                /* 左側が空でなく、かつ落ちているブロックではない */
                if (cells[row][col - 1].blockNum !== null && cells[row][col - 1].blockNum !== fallingBlockNum){
                    return;
                }
            }
        }
    }
    //ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
